(function() {
	'use strict';

	function config(path) {
		return {
			pageMeta: buildPageMeta(path),
			navLinks: buildNavLinks(path)
		};
	}

	function buildPageMeta(path) {
		var title = "Static Site",
			description = "Site Description";

		switch (path) {
			case '/about':
				title = "About Us | " + title;
				description = "About description";
				break;
		}

		return {
			title: title,
			description: description
		};
	}

	function buildNavLinks(path) {
		return [
			{ title: 'About', link: '/about', active: (path === '/about') ? 'is-active' : '' }
		];
	}

	module.exports = config;

})();
