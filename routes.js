
module.exports = function(app) {

	app.get('/', function(req, res) {
		return res.render('pages/home');
	});

	app.get('/about', function(req, res) {
		return res.render('pages/about');
	});

	//Error handling
	app.use(function (err, req, res, next) {
		console.error(err.stack)
		res.status(500).render('pages/error');
	});

	//Page not found
	app.use(function (req, res, next) {
		res.status(404).render('pages/404');
	});

};
